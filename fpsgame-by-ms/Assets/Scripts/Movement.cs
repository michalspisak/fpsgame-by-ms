﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float Movement_Speed;

	// Use this for initialization
	void Start () {
        Movement_Speed = 5f;
        Cursor.visible = false;

    }
	
	// Update is called once per frame
	void Update () {

        transform.Translate(Movement_Speed * Input.GetAxis("Horizontal") * Time.deltaTime, 0f, Movement_Speed * Input.GetAxis("Vertical") * Time.deltaTime);
		
	}
}
