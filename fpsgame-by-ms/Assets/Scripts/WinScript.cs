﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScript : MonoBehaviour {

    public GameObject Target1;
    public GameObject Target2;
    public GameObject Target3;
    public GameObject Target4;
    public GameObject Target5;

    public GameObject winpanel;


    void Start()
    {
        winpanel.SetActive(false);
    }

    void Update()
    {
        Win();
    }

    void Win()
    {
        if(Target1 == null && Target2 == null && Target3 == null && Target4 == null && Target5 == null)
        {
            winpanel.SetActive(true);
            StartCoroutine(Time());
            

        }
    }

    IEnumerator Time()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

}
