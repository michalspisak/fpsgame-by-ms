﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGUIScript : MonoBehaviour {

    public GameObject GUI;
    int changer = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            GUI.SetActive(true);
            changer++;

            if(changer%2==1)
            {
                GUI.SetActive(false);
            }
        }
		
	}
}
