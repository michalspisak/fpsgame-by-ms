﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TargetSniper1 : MonoBehaviour
{

    public float health = 50f;

    // public animator anim for example;

    public void TakeDamage(float amount)
    {
        health -= amount;
        if (health <= 0f)
        {

            Die();
           // StartCoroutine(TimeToDelete());
        }
    }

    void Die()
    {
        Destroy(gameObject);
        // Animation for example if is need :)

    }
    

    IEnumerator TimeToDelete()
    { 
        yield return new WaitForSeconds(2); // TimeForSomething
     

    }
}