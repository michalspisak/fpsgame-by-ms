﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperSpecification : MonoBehaviour
{

    public float damage = 10f;
    public float range = 100f;

    public Camera fpsCam;


    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }


    }

    void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            TargetSniper1 targetsniper1 = hit.transform.GetComponent<TargetSniper1>();
            if (targetsniper1 != null)
            {
                targetsniper1.TakeDamage(damage);
            }


        }
    }
}
