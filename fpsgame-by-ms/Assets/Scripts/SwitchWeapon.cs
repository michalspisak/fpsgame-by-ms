﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchWeapon : MonoBehaviour {

    int switcher = 0;
    public GameObject aimbreak;
    public Camera cam2;




    [SerializeField]
    GameObject pistol, sniper;

	
    void Start()
    {
        //start weapon
        pistol.SetActive(true);
        sniper.SetActive(false);
    }
	// Update is called once per frame
	void Update () {
        

        if (Input.GetKeyDown(KeyCode.Q))
        {

            ChangeWeapon();
            switcher++;
        }

    }

    void ChangeWeapon()
    {  

        //when clicked "Q"
        if (switcher%2==1)
        {
            pistol.SetActive(true);
            sniper.SetActive(false);
            Debug.Log("Pistol - engage");
            if (aimbreak.activeSelf)
            {
                cam2.fieldOfView = 60;
                aimbreak.SetActive(false);
            }
        }

        if (switcher%2==0)
        {
            pistol.SetActive(false);
            sniper.SetActive(true);
            Debug.Log("Sniper - engage");
            
        }
    }
}
