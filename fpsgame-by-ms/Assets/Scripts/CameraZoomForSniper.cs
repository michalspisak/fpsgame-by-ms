﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CameraZoomForSniper : MonoBehaviour
{

    //to zoom need it

    public GameObject Sniper;
    public GameObject AimSniper;
    public Camera cam;


    // Use this for initialization
    void Start()
    {
        AimSniper.SetActive(false);
    }

    // Update is called once per frame  
    void Update()
    {

        if(Input.GetMouseButtonDown(1) && Sniper.activeSelf)
        {
            cam.fieldOfView = 20;
            AimSniper.SetActive(true);

        }

        if (Input.GetMouseButtonUp(1) && Sniper.activeSelf) 
        {
            cam.fieldOfView = 60;
            AimSniper.SetActive(false);
        }


    }
}
